import { faLinkedin, faReplyd } from "@fortawesome/free-brands-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export default function Home() {
  return (
      <main className="min-h-screen flex flex-col justify-center items-center gap-8">
      <header className="text-center">
      <h1 className="text-6xl">Michael McG</h1>
      <p>Architect your future</p>
      </header>
      <section className="flex gap-4">
        <a target="_blank" href="https://www.linkedin.com/in/michaelmcguinnessli/"><FontAwesomeIcon icon={faLinkedin} /></a>
        <a target="_blank" href="mailto:michael.mcguinness@deepvisibility.co"><FontAwesomeIcon icon={faReplyd} /></a>
      </section>
      </main>
      );
}
