"use server";

import { type Span, trace } from "@opentelemetry/api";

export default async function otel<T>(
  fnName: string,
  fn: (...args: any[]) => Promise<T>,
  ...props: any[]
): Promise<T> {
  const tracer = trace.getTracer(fnName);
  return tracer.startActiveSpan(fnName, (span: Span) => {
    try {
      return fn(...props);
    } finally {
      span.end();
    }
  });
}
